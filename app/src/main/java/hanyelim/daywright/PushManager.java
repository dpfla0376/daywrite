package hanyelim.daywright;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by yelim_ on 2015-11-17.
 */
public class PushManager extends Service {

    TimeThread thread;
    public static SQLiteDatabase serviceDB;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            serviceDB = openOrCreateDatabase("myDiary", MODE_WORLD_READABLE, null);
            //databaseCreated = true;
            Log.d("database", "-----created-----");
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e("database", "-----not created-----");
        }
        super.onStartCommand(intent, flags, startId);
        Log.d("PushService", "-----start-----");
        startPushService();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //thread.stop();
        //thread.interrupt();
    }

    void startPushService() {
        thread = new TimeThread(this);
        thread.start();
    }
}

class TimeThread extends Thread {

    private int push_time_hour;
    private int push_time_min;
    private Context mContext;
    private Calendar cal = Calendar.getInstance();

    TimeThread(Context context) {
        mContext = context;
    }

    @Override
    public void run() {

        try {
            Cursor c = PushManager.serviceDB.rawQuery("select push_time from setting", null);
            c.moveToNext();
            String[] str = c.getString(0).split(" ");
            push_time_hour = Integer.valueOf(str[0]);
            push_time_min = Integer.valueOf(str[2]);
            c.close();
        }
        catch (Exception e) {

        }

        while(true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Cursor c = PushManager.serviceDB.rawQuery("select push, push_time from setting", null);
            if (c.getCount() != 0) {
                c.moveToNext();
                if (c.getString(0).compareTo("on") == 0) {
                    String[] str = c.getString(1).split(" ");
                    push_time_hour = Integer.valueOf(str[0]);
                    push_time_min = Integer.valueOf(str[2]);
                    c.close();

                    cal = Calendar.getInstance();

                    Log.d("TimeThread", "----processing-----");
                    Log.d("push_time_hour", push_time_hour + "");
                    Log.d("push_time_min", push_time_min + "");
                    Log.d("CurrentTime", cal.get(Calendar.HOUR_OF_DAY) + "" + cal.get(Calendar.MINUTE) + "");

                    if ((push_time_hour == cal.get(Calendar.HOUR_OF_DAY)) && (push_time_min == cal.get(Calendar.MINUTE))) {
                        /*
                        Cursor c2 = MainActivity.db.rawQuery("select diary from record where record_date='"  + MainActivity.getRecordDate() + "'", null);
                        if(c2.getCount() == 0) {
                            //Notification!!
                            Log.d("Notification", "-----go-----");
                            sendNotification();
                        }
                        else if(c2.getCount() != 0) {
                            c2.moveToNext();
                            if(c2.getString(0) == null || c2.getString(0) == "") {
                                //Notification!!
                                Log.d("Notification", "-----go-----");
                                sendNotification();
                            }
                        }
                        */
                        //Notification!!
                        Log.d("Notification", "-----go-----");
                        sendNotification();

                        try {
                            Thread.sleep(60000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if(c.getString(0).compareTo("off") == 0) {
                    c.close();
                    Log.d("Service", "-----nothing-----");
                }
            }
        }
    }

    public void sendNotification() {
        Intent intent = new Intent(mContext, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        boolean isScreenOn = false;

        PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        if(!pm.isScreenOn()) {
            // wake up the Screen in sleep Mode
            wakeScreen();
            isScreenOn = true;
        }
        else {
            isScreenOn = true;
        }

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.mipmap.daywright)
                .setContentTitle("Notice!")
                .setContentText("일기를 쓰실 시간이에요!")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

        Log.d("Notification", "-----end-----");

    }

    void wakeScreen() {
        PushWakeLock.acquireCpuWakeLock(mContext);
    }

}
