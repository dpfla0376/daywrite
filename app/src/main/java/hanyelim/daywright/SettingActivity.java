package hanyelim.daywright;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yelim_ on 2015-11-10.
 */

public class SettingActivity extends Activity {

    private Calendar c;
    //private SQLiteDatabase db;
    private Cursor c1;

    private Button backButton;
    private Button saveButton;
    private Button settingPushTime;
    private Switch settingPush;
    private Spinner settingLogDate;
    private Switch settingLog;
    private Switch settingLock;
    private Button settingLockPasswd;
    private EditText settingPasswdInput;

    private HashMap map;
    private ArrayList<HashMap> spinnerList;

    private String push;
    private String push_time;
    private String log;
    private String log_date;
    private String lock;
    private String lock_passwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        backButton = (Button) findViewById(R.id.settingBackButton);
        saveButton = (Button) findViewById(R.id.settingSaveButton);
        settingPushTime = (Button) findViewById(R.id.settingPushTime);
        //settingPushTime.setText("00 : 00");
        //settingPushTime.setEnabled(false);
        settingPush = (Switch) findViewById(R.id.settingPushSwitch);
        settingLogDate = (Spinner) findViewById(R.id.settingLogDate);
        settingLog = (Switch) findViewById(R.id.settingLogSwitch);
        settingLock = (Switch) findViewById(R.id.settingLockSwitch);
        settingLockPasswd = (Button) findViewById(R.id.settingLockPasswdButton);

        ArrayList<DayOfWeek> eList = new ArrayList<DayOfWeek>();
        eList.add(new DayOfWeek(DayOfWeek.MONDAY));
        eList.add(new DayOfWeek(DayOfWeek.TUESDAY));
        eList.add(new DayOfWeek(DayOfWeek.WEDNESDAY));
        eList.add(new DayOfWeek(DayOfWeek.THURSDAY));
        eList.add(new DayOfWeek(DayOfWeek.FRIDAY));
        eList.add(new DayOfWeek(DayOfWeek.SATURDAY));
        eList.add(new DayOfWeek(DayOfWeek.SUNDAY));

        ArrayAdapter<DayOfWeek> myAdapter = new MyArrayAdapter(this, R.layout.spinner_image_item, eList);
        settingLogDate.setAdapter(myAdapter);
        settingLogDate.setOnItemSelectedListener(new MyListener());

        //db = openOrCreateDatabase("myDiary", MODE_WORLD_READABLE, null);
        c1 = MainActivity.db.rawQuery("select * from setting", null);
        Log.d("settingNum", c1.getCount()+"");
        if(c1.getCount() == 0) {
            push = "off";
            push_time = "00 : 00";
            log = "off";
            log_date = "Sunday";
            lock = "off";
            lock_passwd = null;
            MainActivity.db.execSQL("insert into setting (push, push_time, log, log_date, lock, lock_passwd) values ("
                    + "'" + push + "', '"
                    + push_time + "', '"
                    + log + "', '"
                    + log_date + "', '"
                    + lock + "', '"
                    + lock_passwd + "');");
        }
        else {
            c1.moveToNext();
            push = c1.getString(0);
            push_time = c1.getString(1);
            log = c1.getString(2);
            log_date = c1.getString(3);
            lock = c1.getString(4);
            lock_passwd = c1.getString(5);
        }
        c1.close();
        setSettings();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveSettings();
            }
        });
        settingPushTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker();
            }
        });
        settingPush.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (!settingPush.isChecked()) {
                        settingPushTime.setEnabled(true);
                        push = "on";
                    } else {
                        settingPushTime.setEnabled(false);
                        push = "off";
                    }
                }
                return false;
            }
        });
        settingLog.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (!settingLog.isChecked()) {
                        settingLogDate.setEnabled(true);
                        log = "on";
                    }
                    else {
                        settingLogDate.setEnabled(false);
                        log = "off";
                    }
                }
                return false;
            }
        });
        settingLock.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (!settingLock.isChecked()) {
                        settingLockPasswd.setEnabled(true);
                        lock = "on";
                    }
                    else {
                        settingLockPasswd.setEnabled(false);
                        lock = "off";
                    }
                }
                return false;
            }
        });
        settingLockPasswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPasswd();
            }
        });
    }

    private void getPasswd() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.passwd_input_dialog, (ViewGroup) findViewById(R.id.newPasswdInput));
        settingPasswdInput = (EditText) layout.findViewById(R.id.newPasswdInput);

        new AlertDialog.Builder(this)
                .setTitle("비밀번호 설정")
                .setView(layout)
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        lock_passwd = settingPasswdInput.getText().toString();
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String s;
            if(hourOfDay >= 0 && hourOfDay < 10) {
                s = "0" + hourOfDay + " : ";
            }
            else {
                s = hourOfDay + " : ";
            }
            if(minute >= 0 && minute < 10) {
                s += "0" + minute;
            }
            else {
                s += minute;
            }
            settingPushTime.setText(s);
            push_time = s;
        }
    };

    private void showTimePicker() {
        c = Calendar.getInstance();
        String[] str = settingPushTime.getText().toString().split(" ");
        //new TimePickerDialog(this, mTimeSetListener, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true).show();
        new TimePickerDialog(this, mTimeSetListener, Integer.valueOf(str[0]), Integer.valueOf(str[2]), true).show();

    }

    private void saveSettings() {
        Intent serviceIntent = new Intent("hanyelim.daywright.startService");
        MainActivity.db.execSQL("update setting set push=" + "'" + push + "', "
                + "push_time='" + push_time + "', "
                + "log='" + log + "', "
                + "log_date='" + log_date + "', "
                + "lock='" + lock + "', "
                + "lock_passwd='" + lock_passwd + "'");
        if(push.compareTo("on") == 0) {
            //stopService(serviceIntent);
            //startService(serviceIntent);
        }
        else if(push.compareTo("off") == 0) {
            //stopService(serviceIntent);
        }
        Toast.makeText(this, "저장되었습니다.", Toast.LENGTH_LONG).show();
        finish();
    }

    private void setSettings() {
        switch (push) {
            case "on" :
                settingPush.setChecked(true);
                settingPushTime.setEnabled(true);
                settingPushTime.setText(push_time);
                break;
            case "off" :
                settingPush.setChecked(false);
                settingPushTime.setEnabled(false);
                settingPushTime.setText(push_time);
                break;
        }

        switch (log) {
            case "on" :
                settingLog.setChecked(true);
                settingLogDate.setEnabled(true);
                settingLogDate.setSelection(DayOfWeek.returnNum(log_date)-1);
                settingLogDate.setEnabled(true);
                break;
            case "off" :
                settingLog.setChecked(false);
                settingLogDate.setEnabled(false);
                Log.d("log_date", log_date);
                Log.d("log_date_id", DayOfWeek.returnID(log_date) + "");
                settingLogDate.setSelection(DayOfWeek.returnNum(log_date) - 1);
                settingLogDate.setEnabled(false);
                break;
        }

        switch (lock) {
            case "on" :
                settingLock.setChecked(true);
                settingLockPasswd.setEnabled(true);
                break;
            case "off" :
                settingLock.setChecked(false);
                settingLockPasswd.setEnabled(false);
                break;
        }

    }

    class MyArrayAdapter extends ArrayAdapter<DayOfWeek> {

        int idLayout;

        public MyArrayAdapter(Context context, int resource, ArrayList<DayOfWeek> objects) {
            super(context, resource, objects);
            idLayout = resource;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            DayOfWeek e = getItem(position);
            int idDrawble = -1;

            switch(e.getType()) {
                case DayOfWeek.MONDAY :
                    idDrawble = R.drawable.monday;
                    break;
                case DayOfWeek.TUESDAY :
                    idDrawble = R.drawable.tuesday;
                    break;
                case DayOfWeek.WEDNESDAY :
                    idDrawble = R.drawable.wednesday;
                    break;
                case DayOfWeek.THURSDAY :
                    idDrawble = R.drawable.thursday;
                    break;
                case DayOfWeek.FRIDAY :
                    idDrawble = R.drawable.friday;
                    break;
                case DayOfWeek.SATURDAY :
                    idDrawble = R.drawable.saturday;
                    break;
                case DayOfWeek.SUNDAY :
                    idDrawble = R.drawable.sunday;
                    break;
            }

            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            ImageView imageView = (ImageView)inflater.inflate(idLayout, parent, false);
            imageView.setImageDrawable(getContext().getResources().getDrawable(idDrawble));

            return imageView;
        }
    }

    class MyListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            DayOfWeek selected = (DayOfWeek)parent.getItemAtPosition(pos);
            log_date = DayOfWeek.returnDay(selected.getType());
            Log.i("DAY", "[" + selected.getType() + "] is just selected");
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // do nothing
        }

    }

}

class DayOfWeek {

    public static final int MONDAY = 1;
    public static final int TUESDAY   = 2;
    public static final int WEDNESDAY = 3;
    public static final int THURSDAY = 4;
    public static final int FRIDAY = 5;
    public static final int SATURDAY = 6;
    public static final int SUNDAY = 7;

    private int type;

    DayOfWeek(int type) {
        this.type = type;
    }

    int getType() {
        return type;
    }

    public static int returnNum(String day) {
        switch(day) {
            case "Monday" :
                return 1;
            case "Tuesday" :
                return 2;
            case "Wednesday" :
                return 3;
            case "Thursday" :
                return 4;
            case "Friday" :
                return 5;
            case "Saturday" :
                return 6;
            case "Sunday" :
                return 7;
        }
        return -1;
    }

    public static int returnID(String day) {
        int idDrawble = -1;
        switch(day) {
            case "Monday" :
                idDrawble = R.drawable.monday;
                break;
            case "Tuesday" :
                idDrawble = R.drawable.tuesday;
                break;
            case "Wednesday" :
                idDrawble = R.drawable.wednesday;
                break;
            case "Thursday" :
                idDrawble = R.drawable.thursday;
                break;
            case "Friday" :
                idDrawble = R.drawable.friday;
                break;
            case "Saturday" :
                idDrawble = R.drawable.saturday;
                break;
            case "Sunday" :
                idDrawble = R.drawable.sunday;
                break;
        }
        return idDrawble;
    }

    public static String returnDay(int id) {
        switch(id) {
            case 1 :
                return "Monday";
            case 2 :
                return "Tuesday";
            case 3 :
                return "Wednesday";
            case 4 :
                return "Thursday";
            case 5 :
                return "Friday";
            case 6 :
                return "Saturday";
            case 7 :
                return "Sunday";
        }
        return null;
    }

}
