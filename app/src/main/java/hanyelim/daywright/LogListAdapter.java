package hanyelim.daywright;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yelim_ on 2015-11-15.
 */
public class LogListAdapter extends BaseAdapter {
    private Context mContext;
    private List<LogListItem> mItems = new ArrayList<LogListItem>();

    public LogListAdapter(Context context) {
        mContext = context;
    }

    public void addItem(LogListItem it) {
        mItems.add(it);
    }

    public void setListItems(List<LogListItem> lit) {
        mItems = lit;
    }

    public int getCount() {
        return mItems.size();
    }

    public Object getItem(int position) {
        return mItems.get(position);
    }

    public boolean areAllItemsSelectable() {
        return false;
    }

    public boolean isSelectable(int position) {
        try {
            return mItems.get(position).isSelectable();
        } catch (IndexOutOfBoundsException ex) {
            return false;
        }
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LogListItemView itemView;
        if(convertView == null) {
            itemView = new LogListItemView(mContext, mItems.get(position));
        }
        else {
            itemView = (LogListItemView) convertView;
        }

        itemView.setText(0, mItems.get(position).getData(0));
        itemView.setText(1, mItems.get(position).getData(1));
        itemView.setText(2, mItems.get(position).getData(2));

        return itemView;
    }
}