package hanyelim.daywright;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by yelim_ on 2015-11-15.
 */
public class LogListItemView extends RelativeLayout {


    private TextView diaryDate;
    private TextView realDate;
    private TextView diary;

    public LogListItemView(Context context, LogListItem aItem) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.log_list_item_layout, this, true);

        diaryDate = (TextView) findViewById(R.id.logDiaryDate);
        diaryDate.setText(aItem.getData(0));
        realDate = (TextView) findViewById(R.id.logRealDate);
        realDate.setText(aItem.getData(1));
        diary = (TextView) findViewById(R.id.logDiary);
        diary.setText(aItem.getData(2));


    }

    public void setText(int index, String data) {
        switch(index) {
            case 0:
                diaryDate.setText(data);
            case 1:
                realDate.setText(data);
            case 2:
                diary.setText(data);
            default:
        }
    }

    public void setText(String[] data) {
        diaryDate.setText(data[0]);
        realDate.setText(data[1]);
        diary.setText(data[2]);
    }

}
