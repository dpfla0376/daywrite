package hanyelim.daywright;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TabHost;

public class LogActivity extends TabActivity {

    private TabHost tabhost;
    private TabHost.TabSpec spec;
    private Intent intent;
    private Resources res;
    private ImageView listNameView;
    private ImageView calendarNameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        tabhost = getTabHost();
        tabhost.getTabWidget().setDividerDrawable(null);
        res = getResources();
        listNameView = new ImageView(this);
        listNameView.setImageDrawable(res.getDrawable(R.drawable.tab_menu_list));
        calendarNameView = new ImageView(this);
        calendarNameView.setImageDrawable(res.getDrawable(R.drawable.tab_menu_calendar));

        intent = new Intent().setClass(this, LogListActivity.class);
        spec = tabhost.newTabSpec("List");
        spec.setIndicator(listNameView);
        spec.setContent(intent);
        tabhost.addTab(spec);

        intent = new Intent().setClass(this, LogCalendarActivity.class);
        spec = tabhost.newTabSpec("Calendar");
        spec.setIndicator(calendarNameView);
        spec.setContent(intent);
        tabhost.addTab(spec);

    }


}
