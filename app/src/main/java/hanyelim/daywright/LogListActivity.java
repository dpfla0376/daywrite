package hanyelim.daywright;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Calendar;

public class LogListActivity extends Activity {

    private Button startDateButton;
    private Button endDateButton;
    private Button searchButton;

    private Calendar cal;
    private int startYear;
    private int startMonth;
    private int startDay;
    private int endYear;
    private int endMonth;
    private int endDay;

    private Dialog datePicker;

    private ListView listView;
    private LogListAdapter adapter;
    private ArrayList<ArrayList<String>> logList;
    //private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_list);

        startDateButton = (Button) findViewById(R.id.startDateButton);
        endDateButton = (Button) findViewById(R.id.endDateButton);
        searchButton = (Button) findViewById(R.id.searchButton);
        listView = (ListView) findViewById(R.id.logListView);
        adapter = new LogListAdapter(this);

        cal = Calendar.getInstance();
        endYear = cal.get(Calendar.YEAR);
        endMonth = cal.get(Calendar.MONTH);
        endDay = cal.get(Calendar.DATE);
        startYear = cal.get(Calendar.YEAR);
        startMonth = cal.get(Calendar.MONTH);
        startDay = 1;

        searchDiary();

        startDateButton.setText(startYear + "년 " + (startMonth+1) + "월 " + startDay + "일");
        startDateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startDatePicker();
            }
        });

        endDateButton.setText(endYear + "년 " + (endMonth + 1) + "월 " + endDay + "일");
        endDateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                endDatePicker();
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                searchDiary();
            }
        });
    }

    private void startDatePicker() {
        datePicker = new DatePickerDialog(this, startDateSetListener, startYear, startMonth, startDay);
        datePicker.show();
    }

    private void endDatePicker() {
        datePicker = new DatePickerDialog(this, endDateSetListener, endYear, endMonth, endDay);
        datePicker.show();
    }

    private void searchDiary() {
        String diaryDate;
        String realDate;
        String realTime;
        String diary;
        adapter = new LogListAdapter(this);
        Cursor[] c = new Cursor[(endYear*10000 + (endMonth+1)*100 + endDay) - (startYear*10000 + (startMonth+1)*100 + startDay) + 1];
        for(int i=startYear*10000 + (startMonth+1)*100 + startDay, j=0; i<=endYear*10000 + (endMonth+1)*100 + endDay; i++, j++) {
            c[j] = MainActivity.db.rawQuery("select * from record where record_date='" + i + "'", null);
            Log.d(i+"", c[j].getCount()+"");
        }
        for(int j=0; j<c.length; j++) {
            if(c[j].getCount() != 0) {
                c[j].moveToNext();
                diaryDate = c[j].getString(0).substring(0, 4) + "년 " + c[j].getString(0).substring(4, 6) + "월 " + c[j].getString(0).substring(6, 8) + "일";
                realDate = c[j].getString(1).substring(0, 4) + "년 " + c[j].getString(1).substring(4, 6) + "월 " + c[j].getString(1).substring(6, 8) + "일";
                realTime = c[j].getString(2).substring(0, 2) + " : " + c[j].getString(2).substring(2, 4);
                diary = c[j].getString(3);
                adapter.addItem(new LogListItem(diaryDate + " 일기", realDate + " " + realTime + " 기록", diary));
                c[j].close();
            }
        }
        listView.setAdapter(adapter);
    }

    private DatePickerDialog.OnDateSetListener startDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            if(year*10000 + (monthOfYear+1)*100 + dayOfMonth <= endYear*10000 + (endMonth+1)*100 + endDay) {
                startYear = year;
                startMonth = monthOfYear;
                startDay = dayOfMonth;
                startDateButton.setText(startYear + "년 " + (startMonth+1) + "월 " + startDay + "일");
            }
            else {
                datePicker.dismiss();
                AlertDialog dialog = createDialogBox();
                dialog.show();
            }
        }
    };

    private DatePickerDialog.OnDateSetListener endDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            if(year*10000 + (monthOfYear+1)*100 + dayOfMonth >= startYear*10000 + (startMonth+1)*100 + startDay) {
                Log.d("startDate", startYear*10000 + (startMonth+1)*100 + startDay+"");
                Log.d("endDate", endYear*10000 + (endMonth+1)*100 + endDay+"");
                Log.d("dialogDate", year * 10000 + (monthOfYear + 1) * 100 + dayOfMonth + "");
                endYear = year;
                endMonth = monthOfYear;
                endDay = dayOfMonth;
                endDateButton.setText(endYear + "년 " + (endMonth+1) + "월 " + endDay + "일");
            }
            else {
                Log.d("startDate", startYear*10000 + (startMonth+1)*100 + startDay+"");
                Log.d("endDate", endYear*10000 + (endMonth+1)*100 + endDay+"");
                Log.d("dialogDate", year * 10000 + (monthOfYear + 1) * 100 + dayOfMonth + "");

                datePicker.dismiss();
                AlertDialog dialog = createDialogBox();
                dialog.show();
            }
        }
    };

    private AlertDialog createDialogBox() {
        AlertDialog.Builder builder = new AlertDialog.Builder(datePicker.getContext());

        builder.setTitle("Warning");
        builder.setMessage("시작일과 종료일을 다시 확인해주세요!");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });

        AlertDialog dialog = builder.create();
        return dialog;

    }

}
