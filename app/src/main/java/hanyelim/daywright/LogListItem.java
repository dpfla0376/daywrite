package hanyelim.daywright;

/**
 * Created by yelim_ on 2015-11-15.
 */
public class LogListItem {

    private String diaryDate;
    private String realDate;
    private String diary;
    private String[] diaryData;

    private boolean mSelectable;

    public LogListItem(String[] obj) {
        diaryDate = obj[0];
        realDate = obj[1];
        diary = obj[2];
        diaryData = obj;
    }

    public LogListItem(String s1, String s2, String s3) {
        diaryData = new String[3];
        diaryData[0] = s1;
        diaryData[1] = s2;
        diaryData[2] = s3;

        diaryDate = s1;
        realDate = s2;
        diary = s3;
    }

    public boolean isSelectable() {
        return mSelectable;
    }

    public void setSelectable(boolean selectable) {
        mSelectable = selectable;
    }

    public String[] getData() {
        return diaryData;
    }

    public String getData(int index) {
        if(diaryData == null || index >= diaryData.length) {
            return null;
        }

        return diaryData[index];
    }

    public void setData(String[] obj) {
        diaryData = obj;
    }

    public int compareTo(LogListItem other) {
        if (diaryData != null) {
            String[] otherData = other.getData();
            if (diaryData.length == otherData.length) {
                for (int i = 0; i < diaryData.length; i++) {
                    if (!diaryData[i].equals(otherData[i])) {
                        return -1;
                    }
                }
            } else {
                return -1;
            }
        } else {
            throw new IllegalArgumentException();
        }

        return 0;
    }

}
