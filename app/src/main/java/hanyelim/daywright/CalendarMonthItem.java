package hanyelim.daywright;

/**
 * Created by yelim_ on 2015-11-22.
 */
public class CalendarMonthItem {
    private int dayValue;
    private String dailyDiary;
    private String realDate;
    private boolean hasDiary;

    public CalendarMonthItem() {
        dailyDiary = null;
        realDate = "";
        hasDiary = false;
    }

    public CalendarMonthItem(int day) {
        dayValue = day;
        dailyDiary = null;
        realDate = "";
        hasDiary = false;
    }

    public int getDay() {
        return dayValue;
    }

    public void setDay(int day) {
        this.dayValue = day;
    }

    public void setDailyDiary(String diary) {
        this.dailyDiary = diary;
        hasDiary = true;
    }

    public String getDailyDiary() {
        return dailyDiary;
    }

    public void setRealDate(String realDate) {
        this.realDate = realDate;
    }

    public String getRealDate() {
        return realDate;
    }

    public boolean hasDiary() {
        return hasDiary;
    }

}
