package hanyelim.daywright;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.text.format.Time;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import java.util.Calendar;

/**
 * Created by yelim_ on 2015-11-22.
 */
public class CalendarMonthAdapter extends BaseAdapter {

    public static final String TAG = "CalendarMonthAdapter";
    public static int oddColor = Color.rgb(225, 225, 225);
    public static int headColor = Color.rgb(12, 32, 158);

    private int selectedPosition = -1;
    private int countColumn = 7;

    private int mStartDay;
    private int startDay;
    private int curYear;
    private int curMonth;

    private int firstDay;
    private int lastDay;

    private CalendarMonthItem[] items;
    private Context mContext;
    private Calendar mCalendar;
    private boolean recreateItems = false;

    public CalendarMonthAdapter(Context context) {
        super();
        mContext = context;
        init();
    }

    public CalendarMonthAdapter(Context context, AttributeSet attrs) {
        super();
        mContext = context;
        init();
    }

    private void init() {
        items = new CalendarMonthItem[7 * 6];
        mCalendar = Calendar.getInstance();
        recalculate();
        resetDayNumbers();
        searchDiary();
    }

    public void recalculate() {

        mCalendar.set(Calendar.DAY_OF_MONTH, 1);

        int dayOfWeek = mCalendar.get(Calendar.DAY_OF_WEEK);
        firstDay = getFirstDay(dayOfWeek);
        Log.d(TAG, "firstDay : " + firstDay);

        mStartDay = mCalendar.getFirstDayOfWeek();
        curYear = mCalendar.get(Calendar.YEAR);
        curMonth = mCalendar.get(Calendar.MONTH);
        lastDay = getMonthLastDay(curYear, curMonth);
        Log.d(TAG, "curYear : " + curYear + ", curMonth : " + curMonth + ", lastDay : " + lastDay);

        int diff = mStartDay - Calendar.SUNDAY - 1;
        startDay = getFirstDayOfWeek();
        Log.d(TAG, "mStartDay : " + mStartDay + ", startDay : " + startDay);

    }

    public void setPreviousMonth() {
        mCalendar.add(Calendar.MONTH, -1);
        recalculate();

        resetDayNumbers();
        selectedPosition = -1;
        searchDiary();
    }

    public void setNextMonth() {
        mCalendar.add(Calendar.MONTH, 1);
        recalculate();

        resetDayNumbers();
        selectedPosition = -1;
        searchDiary();
    }

    public void resetDayNumbers() {
        for (int i = 0; i < 42; i++) {
            int dayNumber = (i+1) - firstDay;
            if (dayNumber < 1 || dayNumber > lastDay) {
                dayNumber = 0;
            }

            items[i] = new CalendarMonthItem(dayNumber);
        }
    }

    private int getFirstDay(int dayOfWeek) {
        int result = 0;

        if (dayOfWeek == Calendar.SUNDAY) {
            result = 0;
        } else if (dayOfWeek == Calendar.MONDAY) {
            result = 1;
        } else if (dayOfWeek == Calendar.TUESDAY) {
            result = 2;
        } else if (dayOfWeek == Calendar.WEDNESDAY) {
            result = 3;
        } else if (dayOfWeek == Calendar.THURSDAY) {
            result = 4;
        } else if (dayOfWeek == Calendar.FRIDAY) {
            result = 5;
        } else if (dayOfWeek == Calendar.SATURDAY) {
            result = 6;
        }

        return result;
    }

    public int getCurYear() {
        return curYear;
    }

    public int getCurMonth() {
        return curMonth;
    }

    public int getNumColumns() {
        return 7;
    }

    public int getCount() {
        return 7 * 6;
    }

    public Object getItem(int position) {
        return items[position];
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(TAG, "getView(" + position + ") called.");

        CalendarMonthItemView itemView;
        if (convertView == null) {
            itemView = new CalendarMonthItemView(mContext);
        } else {
            itemView = (CalendarMonthItemView) convertView;
        }

        GridView.LayoutParams params = new GridView.LayoutParams(
                GridView.LayoutParams.MATCH_PARENT, 100);

        int rowIndex = position / countColumn;
        int columnIndex = position % countColumn;
        Log.d(TAG, "Index : " + rowIndex + ", " + columnIndex);

        itemView.setItem(items[position]);
        itemView.setLayoutParams(params);
        itemView.setPadding(5, 5, 5, 5);
        itemView.setGravity(Gravity.LEFT);

        if (columnIndex == 0) {
            itemView.setTextColor(Color.RED);
        }
        else if(columnIndex == 6) {
            itemView.setTextColor(Color.BLUE);
        }
        else {
            itemView.setTextColor(Color.BLACK);
        }

        Log.d("items["+position+"]", items[position].hasDiary()+"");
        if(items[position].hasDiary() == true) {
            itemView.setBackgroundColor(Color.YELLOW);
        }
        else {
            itemView.setBackgroundColor(Color.WHITE);
        }
/*
        if (position == getSelectedPosition()) {
            itemView.setBackgroundColor(Color.CYAN);
        }
*/
        return itemView;
    }

    public static int getFirstDayOfWeek() {
        int startDay = Calendar.getInstance().getFirstDayOfWeek();

        if (startDay == Calendar.SATURDAY) {
            return Time.SATURDAY;
        } else if (startDay == Calendar.MONDAY) {
            return Time.MONDAY;
        } else {
            return Time.SUNDAY;
        }
    }

    private int getMonthLastDay(int year, int month){
        switch (month) {
            case 0:
            case 2:
            case 4:
            case 6:
            case 7:
            case 9:
            case 11:
                return (31);

            case 3:
            case 5:
            case 8:
            case 10:
                return (30);

            default:
                if(((year%4 == 0) && (year%100 != 0)) || (year%400 == 0) ) {
                    return (29);
                } else {
                    return (28);
                }
        }
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    /**
     * determine if CalendarMonthItem has diary according to its date.
     * if true, put in.
     * */
    private void searchDiary() {
        Cursor[] c = new Cursor[lastDay];
        for(int i=curYear*10000 + (curMonth+1)*100 + 1, j=0; i<=curYear*10000 + (curMonth+1)*100 + lastDay; i++, j++) {
            c[j] = MainActivity.db.rawQuery("select * from record where record_date='" + i + "'", null);
            Log.d(i+"", c[j].getCount()+"");
        }

        for(int i=0, j=0; i<42; i++) {
            if(items[i].getDay() == 0) {
                continue;
            }
            if(c[j].getCount() == 0) {
                j++;
            }
            else if(c[j].getCount() != 0) {
                c[j].moveToNext();
                if (c[j].getString(0).substring(6, 8).compareTo("0" + items[i].getDay()) == 0
                        || c[j].getString(0).substring(6, 8).compareTo("" + items[i].getDay()) == 0) {
                    items[i].setRealDate(c[j].getString(1).substring(0, 4) + "년 "
                            + c[j].getString(1).substring(4, 6) + "월 " + c[j].getString(1).substring(6, 8) + "일 "
                            + c[j].getString(2).substring(0, 2) + " : " + c[j].getString(2).substring(2, 4));
                    items[i].setDailyDiary(c[j].getString(3));
                    c.clone();
                    j++;
                    Log.d("items"+i, "hasDiary");
                }
                else {
                    c[j].moveToPrevious();
                }
            }
        }
    }
}
