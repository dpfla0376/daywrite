package hanyelim.daywright;

import android.view.View;
import android.widget.AdapterView;

/**
 * Created by yelim_ on 2015-11-22.
 */
public interface CalendarOnDateSelectionListener {
    public void onDataSelected(AdapterView parent, View v, int position, long id);
}
