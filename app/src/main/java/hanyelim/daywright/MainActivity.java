package hanyelim.daywright;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SlidingDrawer;
import android.widget.TextView;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_SETTING = 1002;
    public static final int REQUEST_CODE_LOG = 1003;

    private Button editNSaveButton;
    private Button deleteButton;
    private EditText todayWright;
    private TextView todayDate;
    private TextView textCount;
    private Button logButton;
    private Button weekButton;
    private Button settingButton;
    private Button backUpButton;
    private SlidingDrawer slidingDrawer;

    private boolean isEdit;
    public static SQLiteDatabase db;
    private Calendar c;
    private String userEmail;
    private String fileUri;

    private static String record_date;
    private static String real_date;
    private String real_time;

    private Context mContext;
    private boolean isLog_date = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createDatabase();

        editNSaveButton = (Button) findViewById(R.id.editButton);
        deleteButton = (Button) findViewById(R.id.deleteButton);
        todayWright = (EditText) findViewById(R.id.todayWright);
        todayDate = (TextView) findViewById(R.id.todayDate);
        textCount = (TextView) findViewById(R.id.showCurrentTextNum);
        logButton = (Button) findViewById(R.id.logButton);
        weekButton = (Button) findViewById(R.id.weekButton);
        settingButton = (Button) findViewById(R.id.settingButton);
        backUpButton = (Button) findViewById(R.id.backUpButton);
        slidingDrawer = (SlidingDrawer) findViewById(R.id.slide_menu);
        mContext = this;

        isEdit = false;
        todayWright.setFocusable(false);

        setTime();
        Log.d("record_date", record_date);
        Log.d("real_date", real_date);
        Log.d("real_time", real_time);

        todayDate.setText(record_date.substring(0, 4) + "년 "
                + record_date.substring(4, 6) + "월 "
                + record_date.substring(6, 8) + "일 일기");
        isShowWeeklyLog();

        Cursor c1 = db.rawQuery("select diary from record where record_date='"+ record_date + "'", null);
        Log.d("diary", c1.getCount() + "");
        if(c1.getCount() == 0) {
            db.execSQL("insert into record (record_date, real_date, real_time, diary) values ("
                    + "'" + record_date + "', '"
                    + real_date + "', '"
                    + real_time + "', '"
                    + todayWright.getText() + "');");
        }
        else {
            c1.moveToNext();
            todayWright.setText(c1.getString(0));
        }
        c1.close();

        byte[] bytes = null;
        try {
            bytes = todayWright.getText().toString().getBytes("KSC5601");
            int strCount = bytes.length;
            textCount.setText(strCount + " / 300 바이트");
        } catch(UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }

        TextWatcher watcher = new TextWatcher() {
            public void onTextChanged(CharSequence str, int start, int before, int count) {
                byte[] bytes = null;
                try {
                    bytes = str.toString().getBytes("KSC5601");
                    int strCount = bytes.length;
                    textCount.setText(strCount + " / 300 바이트");
                } catch(UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void afterTextChanged(Editable strEditable) {
                String str = strEditable.toString();
                try {
                    byte[] strBytes = str.getBytes("KSC5601");
                    if(strBytes.length > 300) {
                        strEditable.delete(strEditable.length()-2, strEditable.length()-1);
                    }
                } catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
        todayWright.addTextChangedListener(watcher);

        editNSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEdit) {
                    setTime();
                    editNSaveButton.setBackgroundResource(R.drawable.edit);
                    //todayWright.setEnabled(false);
                    todayWright.setFocusable(false);
                    isEdit = false;
                    Cursor c1 = db.rawQuery("select * from record where record_date='" + record_date + "'", null);
                    if (c1.getCount() == 0) {
                        db.execSQL("insert into record (record_date, real_date, real_time, diary) values ("
                                + "'" + record_date + "', '"
                                + real_date + "', '"
                                + real_time + "', '"
                                + todayWright.getText() + "');");
                    } else if (c1.getCount() == 1) {
                        Log.d("attribute", c1.getCount() + "");
                        String diary = todayWright.getText().toString();
                        diary = diary.replaceAll("'", "''");
                        db.execSQL("update record set diary='" + diary + "', "
                                + "real_date='" + real_date + "', "
                                + "real_time='" + real_time + "' where record_date='" + record_date + "'");
                    } else {
                        Log.e("database_recore", "-----ERROR-----");
                    }
                    c1.close();
                } else {
                    editNSaveButton.setBackgroundResource(R.drawable.save);
                    todayWright.setEnabled(true);
                    todayWright.setFocusable(true);
                    todayWright.setFocusableInTouchMode(true);
                    todayWright.requestFocus();
                    isEdit = true;
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(todayWright, 0);
                }

            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialog = createDialogBox();
                dialog.show();
            }
        });

        logButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LogActivity.class);
                startActivityForResult(intent, REQUEST_CODE_LOG);
                slidingDrawer.animateClose();
            }
        });

        weekButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WeekActivity.class);
                startActivity(intent);
                slidingDrawer.animateClose();
            }
        });

        settingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
                startActivityForResult(intent, REQUEST_CODE_SETTING);
                slidingDrawer.animateClose();
            }
        });

        backUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getEmail();
                //makeDBToFile();
                slidingDrawer.animateClose();
            }
        });
    }

    private void makeDBToFile() {
        String mBuf;
        String mRecordDate;
        String mRealDate;
        String mRealTime;
        String mDiary;
        String content = ""; //  figure out how to read your DB and create a string in the format you want

        Cursor c = db.rawQuery("select * from record", null);
        if(c.getCount() != 0) {
            for(int i=0; i<c.getCount(); i++) {
                c.moveToNext();
                mBuf = c.getString(0);
                mRecordDate = mBuf.substring(0, 4) + "년 " + mBuf.substring(4, 6) + "월 " + mBuf.substring(6, 8) + "일";
                mBuf = c.getString(1);
                mRealDate = mBuf.substring(0, 4) + "년 " + mBuf.substring(4, 6) + "월 " + mBuf.substring(6, 8) + "일";
                mBuf = c.getString(2);
                mRealTime = mBuf.substring(0, 2) + " : " + mBuf.substring(2, 4);
                mDiary = c.getString(3);

                content += mRecordDate + "\n" + mRealDate + " " + mRealTime + "\n" + mDiary + "\n\n";
            }
            c.close();
        }

        try {
            File sdCard = Environment.getExternalStorageDirectory();
            File dir = new File(sdCard.getAbsolutePath() + "/DayWrightBackUp/");
            dir.mkdirs();
            File file = new File(dir, "~" + record_date + "_diary.txt");
            fileUri = file.getAbsolutePath();
            Log.d("sdCard", sdCard.getAbsolutePath());
            Log.d("dir", dir.getAbsolutePath());
            Log.d("file", file.getAbsolutePath());
            file.createNewFile();
            setContents(file, content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setContents(File aFile, String aContents) throws FileNotFoundException, IOException {
        if (aFile == null) {
            throw new IllegalArgumentException("File should not be null.");
        }
        if (!aFile.exists()) {
            throw new FileNotFoundException("File does not exist: " + aFile);
        }
        if (!aFile.isFile()) {
            throw new IllegalArgumentException("Should not be a directory: " + aFile);
        }
        if (!aFile.canWrite()) {
            throw new IllegalArgumentException("File cannot be written: " + aFile);
        }

        //declared here only to make visible to finally clause; generic reference
        Writer output = null;
        try {
            //use buffering
            //FileWriter always assumes default encoding is OK!
            output = new BufferedWriter(new FileWriter(aFile));
            output.write(aContents);
        } finally {
            //flush and close both "output" and its underlying FileWriter
            if (output != null) {
                output.close();
            }
        }
        /*

                */
    }

    private void getEmail() {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.passwd_input_dialog, (ViewGroup) findViewById(R.id.newPasswdInput));
        final EditText emailInput = (EditText) layout.findViewById(R.id.newPasswdInput);
        emailInput.setHint("일기를 보낼 E-mail 주소.");
        emailInput.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        emailInput.setFilters(new InputFilter[]{new InputFilter.LengthFilter(30)});
        emailInput.setSingleLine();
        final ImageView emailTitle = (ImageView) layout.findViewById(R.id.newPasswdText);
        //emailTitle.setImageDrawable(getResources().getDrawable(R.drawable.email_text));
        emailTitle.setBackground(getResources().getDrawable(R.drawable.email_text));

        new AlertDialog.Builder(this)
                .setTitle("파일을 보낼 E-mail을 입력하세요.")
                .setView(layout)
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        userEmail = emailInput.getText().toString();
                        Log.d("UserEmail", userEmail);
                        new AsyncTask<String, Void, Boolean>() {
                            @Override
                            protected Boolean doInBackground(String... params) {
                                try {
                                    GMailSender mailSender = new GMailSender("dpfla0376@gmail.com", "hana328200");
                                    makeDBToFile();
                                    mailSender.addAttachment(fileUri);
                                    mailSender.sendMail("[감성일기] " + record_date + " 까지의 일기"
                                            , "[감성일기] 에서 보낸 일기 기록 입니다."
                                            , "dpfla0376@gmail.com"
                                            , userEmail);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Boolean aBoolean) {
                                super.onPostExecute(aBoolean);
                                new AlertDialog.Builder(mContext)
                                        .setTitle("Data BackUp")
                                        .setMessage("BackUp을 완료했습니다. E-mail을 발송했습니다.")
                                        .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                            }
                                        })
                                        .show();
                            }
                        }.execute();

                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
        slidingDrawer.animateClose();
    }

    private void sendEmail() {
        makeDBToFile();

        String[] emailToAddr = new String[1];
        emailToAddr[0] = userEmail;
        Intent it = new Intent(Intent.ACTION_SEND);
        it.setType("plain/text");
        it.putExtra(Intent.EXTRA_EMAIL, emailToAddr);
        it.putExtra(Intent.EXTRA_SUBJECT, "[감성일기] " + real_date + " 까지의 일기");
        it.putExtra(Intent.EXTRA_TEXT, "[감성일기] 에서 보낸 일기 기록 입니다.");
        // 파일첨부
        it.putExtra(Intent.EXTRA_STREAM, fileUri);

        startActivity(it);
    }

    private void setTime() {
        c = Calendar.getInstance();

        if(c.get(Calendar.MONTH) >=0 && c.get(Calendar.MONTH) < 9) {
            real_date = c.get(Calendar.YEAR) + "0" + (c.get(Calendar.MONTH)+1) + "";
        }
        else {
            real_date = c.get(Calendar.YEAR) + "" + (c.get(Calendar.MONTH)+1) + "";
        }

        if(c.get(Calendar.DATE) >=0 && c.get(Calendar.DATE) < 10) {
            real_date += "0" + c.get(Calendar.DATE) +"";
        }
        else {
            real_date += c.get(Calendar.DATE) +"";
        }

        if(c.get(Calendar.HOUR_OF_DAY) >= 6) {
            record_date = real_date;
        }
        else {
            c.add(Calendar.DATE, -1);
            if(c.get(Calendar.MONTH) >=0 && c.get(Calendar.MONTH) < 9) {
                record_date = c.get(Calendar.YEAR) + "0" + (c.get(Calendar.MONTH)+1) + "";
            }
            else {
                record_date = c.get(Calendar.YEAR) + "" + (c.get(Calendar.MONTH)+1) + "";
            }

            if(c.get(Calendar.DATE) >=0 && c.get(Calendar.DATE) < 10) {
                record_date += "0" + c.get(Calendar.DATE) +"";
            }
            else {
                record_date += c.get(Calendar.DATE) +"";
            }
        }

        if(c.get(Calendar.HOUR_OF_DAY) >= 0 && c.get(Calendar.HOUR_OF_DAY) < 10) {
            real_time = "0" + c.get(Calendar.HOUR_OF_DAY);
        }
        else {
            real_time = c.get(Calendar.HOUR_OF_DAY) + "";
        }

        if(c.get(Calendar.MINUTE) >= 0 && c.get(Calendar.MINUTE) < 10) {
            real_time += "0" + c.get(Calendar.MINUTE);
        }
        else {
            real_time += c.get(Calendar.MINUTE);
        }

    }

    private String getDayOfWeek() {
        String dayOfWeek = "";
        c = Calendar.getInstance();
        switch (c.get(Calendar.DAY_OF_WEEK)) {
            case 1:
                //일요일
                dayOfWeek = "Sunday";
                break;
            case 2:
                //월요일
                dayOfWeek = "Monday";
                break;
            case 3:
                //화요일
                dayOfWeek = "Tuesday";
                break;
            case 4:
                //수요일
                dayOfWeek = "Wednesday";
                break;
            case 5:
                //목요일
                dayOfWeek = "Thursday";
                break;
            case 6:
                //금요일
                dayOfWeek = "Friday";
                break;
            case 7:
                //토요일
                dayOfWeek = "Saturday";
                break;
        }
        return dayOfWeek;
    }

    private void isShowWeeklyLog() {
        Cursor cursor = db.rawQuery("select log, log_date from setting", null);
        if(cursor.getCount() != 0) {
            cursor.moveToNext();
            if(cursor.getString(0).compareTo("on") == 0) {
                if(cursor.getString(1).compareTo(getDayOfWeek()) == 0) {
                    if(!isLog_date) {
                        Intent intent = new Intent(getApplicationContext(), WeekActivity.class);
                        startActivity(intent);
                        isLog_date = true;
                    }
                }
            }
        }
    }

    private AlertDialog createDialogBox() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Warning");
        builder.setMessage("오늘의 일기를 모두 지울까요?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                setTime();
                todayWright.setText(null);
                editNSaveButton.setBackgroundResource(R.drawable.edit);
                todayWright.setEnabled(false);
                todayWright.setFocusable(false);
                isEdit = false;
                db.execSQL("update record set diary='" + todayWright.getText() + "', "
                        + "real_date='" + real_date + "', "
                        + "real_time='" + real_time + "' where record_date='" + record_date + "'");
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });

        AlertDialog dialog = builder.create();
        return dialog;

    }

    public static String getRecordDate() {
        return record_date;
    }

    public static String getRealDate() {
        return real_date;
    }

    private void createDatabase() {
        try {
            db = openOrCreateDatabase("myDiary", MODE_WORLD_READABLE, null);

            createRecordTable();
            createSettingTable();
            //databaseCreated = true;
            Log.d("database", "-----created-----");
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e("database", "-----not created-----");
        }
    }

    private void createRecordTable() {
        db.execSQL("create table record ("
                                + "record_date text PRIMARY KEY, "
                                + "real_date text, "
                                + "real_time text, "
                                + "diary text);");
    }

    private void createSettingTable() {
        db.execSQL("create table setting ("
                + "push text, "
                + "push_time text, "
                + "log text, "
                + "log_date text, "
                + "lock text, "
                + "lock_passwd text);");
    }

}
