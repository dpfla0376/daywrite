package hanyelim.daywright;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by yelim_ on 2015-11-22.
 */
public class CalendarMonthItemView extends TextView {

    private CalendarMonthItem item;

    public CalendarMonthItemView(Context context) {
        super(context);
        init();
    }

    public CalendarMonthItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setBackgroundColor(Color.WHITE);
    }


    public CalendarMonthItem getItem() {
        return item;
    }

    public void setItem(CalendarMonthItem item) {
        this.item = item;
        int day = item.getDay();

        if (day != 0) {
            setText(String.valueOf(day));
        } else {
            setText("");
        }

    }
}
