package hanyelim.daywright;

import java.security.AccessController;
import java.security.Provider;

/**
 * Created by yelim_ on 2015-11-29.
 * http://stackoverflow.com/questions/19947685/how-to-send-email-in-background-in-android
 */
public final class JSSEProvider extends Provider
{
    private static final long serialVersionUID = 1L;

    public JSSEProvider()
    {
        super("HarmonyJSSE", 1.0, "Harmony JSSE Provider");
        AccessController.doPrivileged(new java.security.PrivilegedAction<Void>() {
            public Void run() {
                put("SSLContext.TLS",
                        "org.apache.harmony.xnet.provider.jsse.SSLContextImpl");
                put("Alg.Alias.SSLContext.TLSv1", "TLS");
                put("KeyManagerFactory.X509",
                        "org.apache.harmony.xnet.provider.jsse.KeyManagerFactoryImpl");
                put("TrustManagerFactory.X509",
                        "org.apache.harmony.xnet.provider.jsse.TrustManagerFactoryImpl");
                return null;
            }
        });
    }
}