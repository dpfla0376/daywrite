package hanyelim.daywright;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;


public class LogoActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_MAIN = 1001;

    private Button start;
    public static SQLiteDatabase logoDb;
    private Cursor c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);
        /*
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(intent, REQUEST_CODE_MAIN);
        finish();
        */
        try {
            logoDb = openOrCreateDatabase("myDiary", MODE_WORLD_READABLE, null);
            //databaseCreated = true;
            Log.d("database", "-----created-----");
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e("database", "-----not created-----");
        }
/*
        start = (Button) findViewById(R.id.startButton);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c = logoDb.rawQuery("select lock from setting", null);
                if(c.getCount() == 1) {
                    c.moveToNext();
                    if (c.getString(0).compareTo("on") == 0) {
                        Intent intent = new Intent(getApplicationContext(), PasswdActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
                else {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        });
        */
        c = logoDb.rawQuery("select push from setting", null);
        if(c.getCount() == 1) {
            c.moveToNext();
            if(c.getString(0).compareTo("on") == 0) {
                //stopService(new Intent("hanyelim.daywright.startService"));
                startService(new Intent("hanyelim.daywright.startService"));
            }
            else if(c.getString(0).compareTo("off") == 0) {
                stopService(new Intent("hanyelim.daywright.startService"));
            }
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                c = logoDb.rawQuery("select lock from setting", null);
                if(c.getCount() == 1) {
                    c.moveToNext();
                    if (c.getString(0).compareTo("on") == 0) {
                        Intent intent = new Intent(getApplicationContext(), PasswdActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
                else {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 2000);

    }

}
