package hanyelim.daywright;

import android.app.ActionBar;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.Calendar;

/**
 * Created by yelim_ on 2015-11-18.
 */
public class WeekActivity extends Activity {

    private Animation translateTopAnimation;

    private ListView slidingList;
    private LogListAdapter adapter;
    //private LinearLayout slidingListLayout;
    private AbsoluteLayout slidingListLayout;
    private Button startButton;

    private Calendar cal;
    private String startDay;
    private String today;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_week);

        translateTopAnimation = AnimationUtils.loadAnimation(this, R.anim.translate_top);
        SlidingPageAnimationListener animListener = new SlidingPageAnimationListener();
        translateTopAnimation.setAnimationListener(animListener);

        //startButton = (Button) findViewById(R.id.weekStartButton);
        slidingListLayout = (AbsoluteLayout) findViewById(R.id.listContent);
        slidingList = (ListView) findViewById(R.id.weeklyList);
        slidingList.setVerticalScrollBarEnabled(false);

        setWeekDate();
        searchDiary();






        /*
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingListLayout.setVisibility(View.VISIBLE);
                slidingListLayout.startAnimation(translateTopAnimation);
            }
        });
        */
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        int iWidth = View.MeasureSpec.makeMeasureSpec(slidingList.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int iHeight = 0;
        int iCount = adapter.getCount();
        View clsView = null;

        for(int i=0; i<iCount; i++) {
            clsView = adapter.getView(i, clsView, slidingList);
            if(i == 0) {
                clsView.setLayoutParams(new ViewGroup.LayoutParams(iWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            clsView.measure(iWidth, View.MeasureSpec.UNSPECIFIED);
            iHeight += clsView.getMeasuredHeight();
        }
        Log.d("width", iWidth+"");
        Log.d("total height", iHeight+"");
/*
        ViewGroup.LayoutParams clsParam = slidingListLayout.getLayoutParams();
        clsParam.height = 2*iHeight + (slidingList.getDividerHeight() * (iCount - 1));
        slidingListLayout.setLayoutParams(clsParam);
        slidingListLayout.requestLayout();
        Log.d("layout height", slidingListLayout.getHeight() + "");
        */
/*
        clsParam = slidingList.getLayoutParams();
        clsParam.height = 2*iHeight + (slidingList.getDividerHeight() * (iCount - 1));
        //clsParam.height = slidingListLayout.getHeight();
        //clsParam.height = ViewGroup.LayoutParams.MATCH_PARENT;
        Log.d("list param height", clsParam.height+"");
        slidingList.setLayoutParams(clsParam);
        slidingList.requestLayout();
        Log.d("list view height", slidingList.getHeight() + "");
*/
        slidingListLayout.setVisibility(View.VISIBLE);
        slidingListLayout.startAnimation(translateTopAnimation);
    }

    private void setWeekDate() {
        cal = Calendar.getInstance();
        today = MainActivity.getRealDate();
        switch (cal.get(Calendar.DAY_OF_WEEK)) {
            case 1:
                //일요일
                cal.add(Calendar.DATE, -6);
                break;
            case 2:
                //월요일
                break;
            case 3:
                //화요일
                cal.add(Calendar.DATE, -1);
                break;
            case 4:
                //수요일
                cal.add(Calendar.DATE, -2);
                break;
            case 5:
                //목요일
                cal.add(Calendar.DATE, -3);
                break;
            case 6:
                //금요일
                cal.add(Calendar.DATE, -4);
                break;
            case 7:
                //토요일
                cal.add(Calendar.DATE, -5);
                break;
        }
        startDay = cal.get(Calendar.YEAR) + "" + (cal.get(Calendar.MONTH)+1) + "" + cal.get(Calendar.DATE) + "";

    }

    private void searchDiary() {
        String diaryDate;
        String realDate;
        String realTime;
        String diary;
        adapter = new LogListAdapter(this);
        Log.d("today", today);
        Log.d("startDay", startDay);
        Cursor[] c = new Cursor[Integer.valueOf(today) - Integer.valueOf(startDay) + 1];
        for(int i=Integer.valueOf(startDay), j=0; i<=Integer.valueOf(today); i++, j++) {
            c[j] = MainActivity.db.rawQuery("select * from record where record_date='" + i + "'", null);
            Log.d(i + "", c[j].getCount() + "");
        }
        for(int j=0; j<c.length; j++) {
            if(c[j].getCount() != 0) {
                c[j].moveToNext();
                diaryDate = c[j].getString(0).substring(0, 4) + "년 " + c[j].getString(0).substring(4, 6) + "월 " + c[j].getString(0).substring(6, 8) + "일";
                realDate = c[j].getString(1).substring(0, 4) + "년 " + c[j].getString(1).substring(4, 6) + "월 " + c[j].getString(1).substring(6, 8) + "일";
                realTime = c[j].getString(2).substring(0, 2) + " : " + c[j].getString(2).substring(2, 4);
                diary = c[j].getString(3);
                adapter.addItem(new LogListItem(diaryDate + " 일기", realDate + " " + realTime + " 기록", diary));
                Log.d("item", j + "번째 아이템");
                c[j].close();
            }
        }

        slidingList.setAdapter(adapter);
    }

    private class SlidingPageAnimationListener implements Animation.AnimationListener {

        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            slidingListLayout.setVisibility(View.GONE);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    }
}
