package hanyelim.daywright;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by yelim_ on 2015-11-28.
 */
public class PushBootStartService extends BroadcastReceiver {
    String INTENT_ACTION = Intent.ACTION_BOOT_COMPLETED;
    final String TAG = "BOOT_START_SERVICE";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action  = intent.getAction();
        if(action.equals(INTENT_ACTION)) {
            Intent i = new Intent(context, PushManager.class);
            context.startService(i);
        }
    }
}