package hanyelim.daywright;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LogCalendarActivity extends Activity {

    private CalendarMonthView monthView;
    private CalendarMonthAdapter monthViewAdapter;
    private TextView calendarText;
    private Button previous;
    private Button next;


    private int curYear;
    private int curMonth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_calendar);

        monthView = (CalendarMonthView) findViewById(R.id.monthView);
        monthViewAdapter = new CalendarMonthAdapter(this);
        monthView.setAdapter(monthViewAdapter);

        monthView.setOnDataSelectionListener(new CalendarOnDateSelectionListener() {
            public void onDataSelected(AdapterView parent, View v, int position, long id) {
                CalendarMonthItem curItem = (CalendarMonthItem) monthViewAdapter.getItem(position);
                int day = curItem.getDay();

                if(curItem.hasDiary()) {
                    //여기에 diary 내용 보여줄 dialog 띄우기
                    getDailyDiary(curItem);
                    Log.d("CalendarMonthView", "Selected : " + day);
                }
            }
        });

        calendarText = (TextView) findViewById(R.id.calendarDateText);
        setMonthText();

        previous = (Button) findViewById(R.id.calendarPrevButton);
        previous.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                monthViewAdapter.setPreviousMonth();
                monthViewAdapter.notifyDataSetChanged();
                setMonthText();
            }
        });

        next = (Button) findViewById(R.id.calendarNextButton);
        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                monthViewAdapter.setNextMonth();
                monthViewAdapter.notifyDataSetChanged();
                setMonthText();
            }
        });

    }

    private void setMonthText() {
        curYear = monthViewAdapter.getCurYear();
        curMonth = monthViewAdapter.getCurMonth();
        calendarText.setText(curYear + "년 " + (curMonth + 1) + "월");
    }

    private void getDailyDiary(CalendarMonthItem curItem) {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.log_list_item_layout, (ViewGroup) findViewById(R.id.logListContent));
        TextView diaryDate = (TextView) layout.findViewById(R.id.logDiaryDate);
        TextView realDate = (TextView) layout.findViewById(R.id.logRealDate);
        TextView diary = (TextView) layout.findViewById(R.id.logDiary);
        LinearLayout diaryContent = (LinearLayout) layout.findViewById(R.id.logListContent);

        diaryDate.setText(curYear + "년 " + (curMonth+1) + "월 " + curItem.getDay() + "일 일기");
        realDate.setText(curItem.getRealDate() + " 기록");
        diary.setText(curItem.getDailyDiary());
        diaryContent.setBackgroundResource(0);

        new AlertDialog.Builder(this)
                .setTitle(curYear + "년 " + (curMonth + 1) + "월 " + curItem.getDay() + "일 일기")
                .setView(layout)
                .setPositiveButton("닫기", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

}
