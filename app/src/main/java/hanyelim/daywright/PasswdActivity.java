package hanyelim.daywright;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by yelim_ on 2015-11-16.
 */
public class PasswdActivity extends Activity {

    private ImageView input1;
    private ImageView input2;
    private ImageView input3;
    private ImageView input4;

    private Button number1;
    private Button number2;
    private Button number3;
    private Button number4;
    private Button number5;
    private Button number6;
    private Button number7;
    private Button number8;
    private Button number9;
    private Button number0;

    private Button back;

    private Intent intent;
    private String input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passwd);

        input = "";

        input1 = (ImageView) findViewById(R.id.input1);
        input2 = (ImageView) findViewById(R.id.input2);
        input3 = (ImageView) findViewById(R.id.input3);
        input4 = (ImageView) findViewById(R.id.input4);

        number1 = (Button) findViewById(R.id.number1Button);
        number2 = (Button) findViewById(R.id.number2Button);
        number3 = (Button) findViewById(R.id.number3Button);
        number4 = (Button) findViewById(R.id.number4Button);
        number5 = (Button) findViewById(R.id.number5Button);
        number6 = (Button) findViewById(R.id.number6Button);
        number7 = (Button) findViewById(R.id.number7Button);
        number8 = (Button) findViewById(R.id.number8Button);
        number9 = (Button) findViewById(R.id.number9Button);
        number0 = (Button) findViewById(R.id.number0Button);

        back = (Button) findViewById(R.id.passwdBackButton);

        number1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input += "1";
                switch (input.length()) {
                    case 1:
                        input1.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 2:
                        input2.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 3:
                        input3.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 4:
                        input4.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        compareToPasswd();
                        break;
                }
            }
        });

        number2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input += "2";
                switch (input.length()) {
                    case 1:
                        input1.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 2:
                        input2.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 3:
                        input3.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 4:
                        input4.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        compareToPasswd();
                        break;
                }
            }
        });

        number3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input += "3";
                switch (input.length()) {
                    case 1:
                        input1.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 2:
                        input2.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 3:
                        input3.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 4:
                        input4.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        compareToPasswd();
                        break;
                }
            }
        });

        number4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input += "4";
                switch (input.length()) {
                    case 1:
                        input1.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 2:
                        input2.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 3:
                        input3.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 4:
                        input4.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        compareToPasswd();
                        break;
                }
            }
        });

        number5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input += "5";
                switch (input.length()) {
                    case 1:
                        input1.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 2:
                        input2.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 3:
                        input3.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 4:
                        input4.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        compareToPasswd();
                        break;
                }
            }
        });

        number6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input += "6";
                switch (input.length()) {
                    case 1:
                        input1.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 2:
                        input2.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 3:
                        input3.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 4:
                        input4.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        compareToPasswd();
                        break;
                }
            }
        });

        number7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input += "7";
                switch (input.length()) {
                    case 1:
                        input1.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 2:
                        input2.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 3:
                        input3.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 4:
                        input4.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        compareToPasswd();
                        break;
                }
            }
        });

        number8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input += "8";
                switch (input.length()) {
                    case 1:
                        input1.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 2:
                        input2.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 3:
                        input3.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 4:
                        input4.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        compareToPasswd();
                        break;
                }
            }
        });

        number9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input += "9";
                switch (input.length()) {
                    case 1:
                        input1.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 2:
                        input2.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 3:
                        input3.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 4:
                        input4.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        compareToPasswd();
                        break;
                }
            }
        });

        number0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input += "0";
                switch (input.length()) {
                    case 1:
                        input1.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 2:
                        input2.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 3:
                        input3.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        break;
                    case 4:
                        input4.setImageDrawable(getResources().getDrawable(R.drawable.passwd_input));
                        compareToPasswd();
                        break;
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (input.length()) {
                    case 1:
                        input = input.substring(0, 0);
                        input1.setImageDrawable(getResources().getDrawable(R.drawable.passwd_no_input));
                        break;
                    case 2:
                        input = input.substring(0, 1);
                        input2.setImageDrawable(getResources().getDrawable(R.drawable.passwd_no_input));
                        break;
                    case 3:
                        input = input.substring(0, 2);
                        input3.setImageDrawable(getResources().getDrawable(R.drawable.passwd_no_input));
                        break;
                    case 4:
                        input = input.substring(0, 3);
                        input4.setImageDrawable(getResources().getDrawable(R.drawable.passwd_no_input));
                        break;
                }
            }
        });


    }

    public void compareToPasswd() {
        Cursor c = LogoActivity.logoDb.rawQuery("select lock_passwd from setting", null);
        if(c.getCount() == 1) {
            c.moveToNext();
            if(input.compareTo(c.getString(0)) == 0) {
                intent = new Intent().setClass(this, MainActivity.class);
                startActivityForResult(intent, LogoActivity.REQUEST_CODE_MAIN);
                finish();
            }
            else {
                input1.setImageDrawable(getResources().getDrawable(R.drawable.passwd_no_input));
                input2.setImageDrawable(getResources().getDrawable(R.drawable.passwd_no_input));
                input3.setImageDrawable(getResources().getDrawable(R.drawable.passwd_no_input));
                input4.setImageDrawable(getResources().getDrawable(R.drawable.passwd_no_input));
                input = "";
                Toast.makeText(this, "비밀번호가 맞지 않습니다.", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            Log.e("Passwd", "not existed!");
        }



    }
}
