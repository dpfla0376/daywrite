package hanyelim.daywright;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by yelim_ on 2015-11-10.
 */
public class SettingActivity extends Activity {

    private Calendar c;
    private SQLiteDatabase db;
    private Cursor c1;

    private Button backButton;
    private Button saveButton;
    private Button settingPushTime;
    private Switch settingPush;
    private Spinner settingLogDate;
    private Switch settingLog;
    private Switch settingLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        backButton = (Button) findViewById(R.id.settingBackButton);
        saveButton = (Button) findViewById(R.id.settingSaveButton);
        settingPushTime = (Button) findViewById(R.id.settingPushTime);
        settingPushTime.setText("00 : 00");
        settingPushTime.setEnabled(false);
        settingPush = (Switch) findViewById(R.id.settingPushSwitch);
        settingLogDate = (Spinner) findViewById(R.id.settingLogDate);
        settingLog = (Switch) findViewById(R.id.settingLogSwitch);
        settingLock = (Switch) findViewById(R.id.settingLockSwitch);
        db = openOrCreateDatabase("myDiary", MODE_WORLD_READABLE, null);
        c1 = db.rawQuery("select * from setting", null);
        if(c1.getCount() == 0) {
            db.execSQL("insert into record (push, push_time, log, log_date, lock, lock_passwd) values ("
                    + "'" + 1 + "', '"
                    + 2 + "', '"
                    + 3 + "', '"
                    + 4 + "', '"
                    + 5 + "', '"
                    + 6 + "');");
        }
        else {

        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveSettings();
            }
        });
        settingPushTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker();
            }
        });
        settingPush.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (!settingPush.isChecked()) {
                        settingPushTime.setEnabled(true);
                    }
                    else {
                        settingPushTime.setEnabled(false);
                    }
                }
                return false;
            }
        });
    }

    private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String s;
            if(hourOfDay >= 0 && hourOfDay < 10) {
                s = "0" + hourOfDay + " : ";
            }
            else {
                s = hourOfDay + " : ";
            }
            if(minute >= 0 && minute < 10) {
                s += "0" + minute;
            }
            else {
                s += minute;
            }
            settingPushTime.setText(s);
        }
    };

    private void showTimePicker() {
        c = Calendar.getInstance();
        String[] str = settingPushTime.getText().toString().split(" ");
        //new TimePickerDialog(this, mTimeSetListener, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true).show();
        new TimePickerDialog(this, mTimeSetListener, Integer.valueOf(str[0]), Integer.valueOf(str[2]), true).show();

    }

    private void saveSettings() {
        db.execSQL("select * from setting");
    }
}
