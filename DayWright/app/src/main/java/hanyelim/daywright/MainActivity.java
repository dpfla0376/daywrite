package hanyelim.daywright;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SlidingDrawer;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_SETTING = 1002;

    private Button editNSaveButton;
    private Button deleteButton;
    private EditText todayWright;
    private TextView textCount;
    private Button logButton;
    private Button weekButton;
    private Button settingButton;
    private SlidingDrawer slidingDrawer;

    private boolean isEdit;
    private SQLiteDatabase db;
    private Calendar c;

    private String record_date;
    private String real_date;
    private String real_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createDatabase();

        editNSaveButton = (Button) findViewById(R.id.editButton);
        deleteButton = (Button) findViewById(R.id.deleteButton);
        todayWright = (EditText) findViewById(R.id.todayWright);
        textCount = (TextView) findViewById(R.id.showCurrentTextNum);
        logButton = (Button) findViewById(R.id.logButton);
        weekButton = (Button) findViewById(R.id.weekButton);
        settingButton = (Button) findViewById(R.id.settingButton);
        slidingDrawer = (SlidingDrawer) findViewById(R.id.slide_menu);

        isEdit = false;
        todayWright.setFocusable(false);

        setTime();
        Log.d("record_date", record_date);
        Log.d("real_date", real_date);
        Log.d("real_time", real_time);

        Cursor c1 = db.rawQuery("select diary from record where record_date='"+ record_date + "'", null);
        Log.d("diary", c1.getCount() + "");
        if(c1.getCount() == 0) {
            db.execSQL("insert into record (record_date, real_date, real_time, diary) values ("
                    + "'" + record_date + "', '"
                    + real_date + "', '"
                    + real_time + "', '"
                    + todayWright.getText() + "');");
        }
        else {
            c1.moveToNext();
            todayWright.setText(c1.getString(0));
        }
        TextWatcher watcher = new TextWatcher() {
            public void onTextChanged(CharSequence str, int start, int before, int count) {
                byte[] bytes = null;
                try {
                    bytes = str.toString().getBytes("KSC5601");
                    int strCount = bytes.length;
                    textCount.setText(strCount + " / 300 바이트");
                } catch(UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void afterTextChanged(Editable strEditable) {
                String str = strEditable.toString();
                try {
                    byte[] strBytes = str.getBytes("KSC5601");
                    if(strBytes.length > 300) {
                        strEditable.delete(strEditable.length()-2, strEditable.length()-1);
                    }
                } catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
        todayWright.addTextChangedListener(watcher);

        editNSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEdit) {
                    editNSaveButton.setBackgroundResource(R.drawable.edit);
                    //todayWright.setEnabled(false);
                    todayWright.setFocusable(false);
                    isEdit = false;
                    Cursor c1 = db.rawQuery("select * from record where record_date='"+ record_date + "'", null);
                    if(c1.getCount() == 0) {
                        db.execSQL("insert into record (record_date, real_date, real_time, diary) values ("
                                + "'" + record_date + "', '"
                                + real_date + "', '"
                                + real_time + "', '"
                                + todayWright.getText() + "');");
                    }
                    else if (c1.getCount() == 1) {
                        Log.d("attribute", c1.getCount()+"");
                        db.execSQL("update record set diary='" + todayWright.getText() + "' where record_date='" + record_date +"'");
                    }
                    else {
                        Log.e("database_recore", "-----ERROR-----");
                    }
                } else {
                    editNSaveButton.setBackgroundResource(R.drawable.save);
                    todayWright.setEnabled(true);
                    todayWright.setFocusable(true);
                    todayWright.setFocusableInTouchMode(true);
                    todayWright.requestFocus();
                    isEdit = true;
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(todayWright, 0);
                }

            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialog = createDialogBox();
                dialog.show();
            }
        });

        settingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
                startActivityForResult(intent, REQUEST_CODE_SETTING);
                slidingDrawer.animateClose();
            }
        });
    }

    private void setTime() {
        c = Calendar.getInstance();
        real_date = c.get(Calendar.YEAR) + "" + (c.get(Calendar.MONTH)+1) + "" + c.get(Calendar.DATE) +"";
        real_time = c.get(Calendar.HOUR_OF_DAY) + "" + c.get(Calendar.MINUTE) + "";

        if(c.get(Calendar.HOUR_OF_DAY) >= 6) {
            record_date = real_date;
        }
        else {
            c.add(Calendar.DATE, -1);
            record_date = c.get(Calendar.YEAR) + "" + (c.get(Calendar.MONTH)+1) + "" + c.get(Calendar.DATE) +"";
        }

    }

    private AlertDialog createDialogBox() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Warning");
        builder.setMessage("오늘의 일기를 모두 지울까요?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                todayWright.setText(null);
                editNSaveButton.setBackgroundResource(R.drawable.edit);
                todayWright.setEnabled(false);
                todayWright.setFocusable(false);
                isEdit = false;
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });

        AlertDialog dialog = builder.create();
        return dialog;

    }

    private void createDatabase() {
        try {
            db = openOrCreateDatabase("myDiary", MODE_WORLD_READABLE, null);

            createRecordTable();
            createSettingTable();
            //databaseCreated = true;
            Log.d("database", "-----created-----");
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e("database", "-----not created-----");
        }
    }

    private void createRecordTable() {
        db.execSQL("create table record ("
                                + "record_date text PRIMARY KEY, "
                                + "real_date text, "
                                + "real_time text, "
                                + "diary text);");
    }

    private void createSettingTable() {
        db.execSQL("create table setting ("
                + "push text, "
                + "push_time text, "
                + "log text, "
                + "log_date text, "
                + "lock text, "
                + "lock_passwd text);");
    }

/*
    private class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table setting ("
                    + "push text, "
                    + "push_time text, "
                    + "log text, "
                    + "log_date text, "
                    + "lock text, "
                    + "lock_passwd text);");

            db.execSQL("create table record ("
                    + "record_date text PRIMARY KEY, "
                    + "real_date text, "
                    + "real_time text, "
                    + "diary text);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }*/
}
